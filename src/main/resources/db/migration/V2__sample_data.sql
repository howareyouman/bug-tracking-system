insert into issue_status (name)
values ('open'),
       ('resolved'),
       ('closed'),
       ('reopened'),
       ('in progress');

insert into issue_status_graph (from_id, to_id, transition_name)
values (1, 5, 'Start Progress'),
       (1, 2, 'Resolve issue'),
       (1, 3, 'Close Issue'),
       (2, 3, 'Close Issue'),
       (2, 4, 'Reopen Issue'),
       (3, 4, 'Reopen Issue'),
       (4, 2, 'Resolve Issue'),
       (4, 3, 'Close Issue'),
       (4, 5, 'Start Process'),
       (5, 1, 'Stop Process'),
       (5, 2, 'Resolve Issue'),
       (5, 3, 'Close Issue');



create table issue_status (
  id   serial primary key,
  name varchar(50) not null unique
);

create table issue_status_graph (
  id              bigserial primary key,
  from_id         int                      not null references issue_status (id),
  to_id           int                      not null references issue_status (id),
  transition_name varchar(50)              not null,
  added_time      timestamp with time zone not null default now(),
  unique (from_id, to_id)
);

create table action (
  id   serial primary key,
  name varchar(100) not null unique
);

create table issue (
  id               bigserial primary key,
  name             varchar(200)             not null,
  description      varchar(500)             not null,
  status_id        int                      not null references issue_status (id),
  last_modify_time timestamp with time zone not null default now(),
  request_mark     bigint                   not null
);


create sequence issue_request_mark_seq;

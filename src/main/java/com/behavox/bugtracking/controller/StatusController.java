package com.behavox.bugtracking.controller;


import com.behavox.bugtracking.command.status.CreateStatusRequest;
import com.behavox.bugtracking.dao.StatusDao;
import com.behavox.bugtracking.entity.IssueStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping(value = "/status",
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class StatusController {

    private final StatusDao statusDao;
    private final TransactionTemplate transactionTemplate;

    public StatusController(@NotNull StatusDao statusDao,
                            @NotNull TransactionTemplate transactionTemplate) {
        this.statusDao = statusDao;
        this.transactionTemplate = transactionTemplate;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<IssueStatus> getAll() {
        return statusDao.getAll();
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public void add(@Valid @RequestBody CreateStatusRequest request) {
        transactionTemplate.execute(transactionStatus -> {
            statusDao.add(request.getName());
            return "";
        });
    }

    @RequestMapping(value = "/addTransition", method = RequestMethod.POST)
    public void addTransition() {

    }

    @RequestMapping(value = "/removeTransition", method = RequestMethod.POST)
    public void removeTransition() {

    }
}

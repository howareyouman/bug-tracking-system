package com.behavox.bugtracking.controller;

import com.behavox.bugtracking.entity.Issue;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/issue",
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class IssueController {

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String create() {
        return "Greetings from Spring Boot!";
    }

    @RequestMapping(value = "/getById", method = RequestMethod.GET)
    public Issue getById() {
        return null;
    }

    @RequestMapping(value = "/changeStatus", method = RequestMethod.POST)
    public void changeIssueStatus() {

    }

    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public void removeIssue() {

    }

}

package com.behavox.bugtracking.controller;


import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/action",
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ActionController {

    @RequestMapping(value = "/getAllAvailable", method = RequestMethod.POST)
    public void getAllAvailableActions() {

    }

    @RequestMapping(value = "/addAction", method = RequestMethod.POST)
    public void addAction() {

    }

}

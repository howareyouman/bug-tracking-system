package com.behavox.bugtracking.entity;

import org.joda.time.DateTime;

import javax.validation.constraints.NotNull;

public class Issue {
    @NotNull
    private final Long id;

    @NotNull
    private final String name;

    @NotNull
    private final String description;

    @NotNull
    private final IssueStatus issueStatus;

    @NotNull
    private final DateTime lastModified;

    @NotNull
    private final Long requestMark;

    public Issue(Long id,
                 String name,
                 String description,
                 IssueStatus issueStatus,
                 DateTime lastModified,
                 Long requestMark) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.issueStatus = issueStatus;
        this.lastModified = lastModified;
        this.requestMark = requestMark;
    }
}

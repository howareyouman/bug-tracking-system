package com.behavox.bugtracking.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({
        DaoConfiguration.class,
        ControllerConfiguration.class,
        ServiceConfiguration.class,
        CommandConfiguration.class
})
public class ApplicationConfiguration {

}

package com.behavox.bugtracking.configuration;

import com.behavox.bugtracking.controller.StatusController;
import com.behavox.bugtracking.dao.StatusDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.support.TransactionTemplate;

@Configuration
@Import({DaoConfiguration.class})
public class ControllerConfiguration {

    @Bean
    StatusController statusController(StatusDao statusDao, TransactionTemplate transactionTemplate) {
        return new StatusController(statusDao, transactionTemplate);
    }
}

package com.behavox.bugtracking.configuration;

import com.behavox.bugtracking.dao.StatusDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class DaoConfiguration {

    @Bean
    StatusDao statusDao(DataSource dataSource) {
        return new StatusDao(dataSource);
    }
}

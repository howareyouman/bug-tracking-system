package com.behavox.bugtracking.dao;


import com.behavox.bugtracking.entity.IssueStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import javax.validation.constraints.NotNull;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static java.util.Objects.requireNonNull;

public class StatusDao {

    private static final RowMapper<IssueStatus> ISSUE_STATUS_ROW_MAPPER = new IssueStatusRowMapper();

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public StatusDao(@NotNull DataSource dataSource) {
        requireNonNull(dataSource, "dataSource");
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<IssueStatus> getAll() {
        return jdbcTemplate.query("select id, name from issue_status", ISSUE_STATUS_ROW_MAPPER);
    }

    public void add(@NotNull String name) {
        requireNonNull(name, "name");
        jdbcTemplate.update("insert into issue_status (name) values (:name)",
                new MapSqlParameterSource("name", name));
    }


    private static class IssueStatusRowMapper implements RowMapper<IssueStatus> {

        @Override
        public IssueStatus mapRow(ResultSet resultSet, int i) throws SQLException {
            return new IssueStatus(resultSet.getInt("id"), resultSet.getString("name"));
        }
    }

}

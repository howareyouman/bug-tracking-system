package com.behavox.bugtracking.command.status;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class CreateStatusRequest {
    @NotNull
    private final String name;

    @JsonCreator
    public CreateStatusRequest(@JsonProperty("name") String name) {
        this.name = name;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }
}

package com.behavox.bugtracking;

import com.behavox.bugtracking.configuration.ApplicationConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({ApplicationConfiguration.class})
public class BugTrackingApplication {
    public static void main(String[] args) {
        SpringApplication.run(BugTrackingApplication.class, args);
    }
}
